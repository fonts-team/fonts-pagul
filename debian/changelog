fonts-pagul (1.0-9) unstable; urgency=medium

  * Team upload.
  * debian/clean: Purge TTFfiles/ dir to prevent FTBFS on double
    build. (Closes: #1044340, #1049667)
  * debian/control: Bump Standards-Version to 4.6.2.
  * debian/control: Bump debhelper compat to v13.
  * debian/patches: Refresh patch with "gbp pq" to fix broken patch
    with last line that does not include "clean" target.

 -- Boyuan Yang <byang@debian.org>  Fri, 25 Aug 2023 09:52:29 -0400

fonts-pagul (1.0-8) unstable; urgency=low

  * Team upload.
  * debian/control:
    + Set Maintainer to Debian Fonts Task Force.
    + Switched to debhelper-compat.
    + Updated Standards-Version to 4.5.0
    + Updated Build-Depends.
    + Added 'Rules-Requires-Root' field.
    + Updated Vcs-* URLs.
  * debian/rules:
    + Use simple dh.
  * Updated debian/copyright.
  * Added debian/gitlab-ci.yml.

 -- Kartik Mistry <kartik@debian.org>  Tue, 28 Apr 2020 11:30:15 +0530

fonts-pagul (1.0-7) unstable; urgency=low

  * Upload to unstable.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 11 May 2013 11:00:22 +0530

fonts-pagul (1.0-6) experimental; urgency=low

  * Bumped Standards-Version to 3.9.4, this didn't require any change to
    package source.
  * Removed deprecated DM-Upload-Allowed field.
  * Marked the package as Multi-Arch foreign to allow proper installation
    on multi-arch systems.
  * Converted package to CDBS
  * Removed debian/preinst file, cleaning up of old file should be handled
    by dpkg itself.
  * Introduced control.in template file.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Thu, 07 Mar 2013 23:19:55 +0530

fonts-pagul (1.0-5) unstable; urgency=low

  * Team upload.
  [Kartik Mistry]
  * debian/control:
    + Set priority to 'optional' from 'extra' to fix policy violation for
      fonts-indic (Policy: Section 2.5: Priorities)
    + Bumped Standards-Version to 3.9.3
  * debian/copyright:
    + Updated for copyright-format 1.0
    + Wrapped up text to 80 characters
  [Vasudev Kamath]
  * debian/copyright
    + Extended copyright year for debian folder to 2011-2012

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 24 Mar 2012 23:17:56 +0530

fonts-pagul (1.0-4) unstable; urgency=low

  [Muneeb Shaikh]
  * debian/90-fonts-pagul.conf -> debian/65-0-fonts-pagul.conf
    + Renamed configuration file to override nonlatin.conf
  [Vasudev Kamath]
  * debian/preinst:
    + Added condition to remove old 90-fonts-pagul.conf
  * debian/links:
    + Links are changed to 65-0-fonts-pagul.conf from 90-fonts-pagul.conf

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 28 Jan 2012 22:53:23 +0530

fonts-pagul (1.0-3) unstable; urgency=low

  [Vasudev Kamath]
  * Language code used in fontconfig file was ta instead of saz fixed it.
  * debian/install:
    + Font was directly installing to truetype folder instead of pagul.
  * debian/preinst:
    + Added this file to remove stray Pagul.ttf in truetype folder during
      upgrade.
  * debian/control:
    + Added fontforge dependency.
  * debian/patches:
    + Added Makefile and generate.pe patches to build font from source.

  [Kartik Mistry]
  * debian/control:
    + Set: DM-Upload-Allowed: yes.
  * debian/install:
    + Fixed typo.
  * debian/patches:
    + Fixed makefile.patch for permission issue.
    + Added patch headers.
  * debian/copyright:
    + Fixed unversioned DEP-5 URL.

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Fri, 30 Dec 2011 15:04:00 +0530

fonts-pagul (1.0-2) unstable; urgency=low

  [Vasudev Kamath]
  * debian/control
    + Vcs* fields now point to Debian-IN git repository
  * debian/copyright
    + Introduced "." in all empty lines to make lintian happy

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Thu, 06 Oct 2011 18:22:58 +0530

fonts-pagul (1.0-1) unstable; urgency=low

  [Vasudev Kamath]
  * Initial release (Closes: #623944)

  [Kartik Mistry]
  * Fixed watch and added get-orig-source

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Fri, 06 May 2011 12:40:24 +0530
